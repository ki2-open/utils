# @ki2/utils

## Helper functions

- `fill.ts`
  - `fillBefore`
  - `fillAfter`
  - `fill0` (`fillBefore` with `0`)
- `filterArrays.ts`
  - `firstNotNull`
  - `lastNotNull`
- `genCode.ts`
  - `genNumericalCode`
- `loadashy.ts`
  - `keys`
  - `values`
  - `each`
  - `some`
  - `every`
  - `extend`
  - `omit`
  - `pick`
  - `isMatch`
  - `isEmpty`
  - `merge`
- `shuffle.ts`
  - `shuffle`
- `strvalues.ts`
  - `value2str`
  - `str2value`
- `time.ts`
  - `diffDate`
  - `isFirstNewer`
  - `isFirstOlder`
  - `isSameDate`
  - `sortOlderFirst`
  - `sortNewerFirst`
  - `removeTime`
- `upperFirstLetter.ts`
  - `upperFirstLetter`
- `urlify.ts`
  - `urlify`

## Validation functions

- `basetypescheck.ts`
  - `isNumber`
  - `isString`
  - `isBoolean`
  - `isObject`
  - `isStrictObject` (not an array)
  - `isBigInt`
  - `isSymbol`
  - `isFunction`
  - `isArray`
  - `isUndefined`
  - `isNull`
  - `isPromise`
- `exist.ts`
  - `exist`
- `isValidEmail.ts`
  - `isValidEmail`
- `isValidUUID.ts`
  - `isValidUUID`
- `passwords.ts`
  - `hasLowerCaseLetters`
  - `hasUpperCaseLetters`
  - `hasNumbers`
  - `hasSpecialLetters`
  - `isShortPassword`
  - `isStrongPassword`

## Types

```ts
// base.ts
type Omit<T, K> = Pick<T, Exclude<typeof T, K>>;

type Nullable<T> = T | null;
type Disableable<T> = Nullable<T> | false;
type Nonable<T> = T | "@none";
```

```ts
// db.ts
type DBDate = string | Date;
type DBEmail = string;
type DBPassword = string;
```

```ts
// nr.ts
type NR<T> = T | undefined;
type NRString = NR<string>;
type NRNumber = NR<number>;
```

## Examples

### `fillBefore`

Fill a string with a _fill string_. The fill string is added **before** the base string until the total length is equal to the fill length (or possibly superior if the fill string has more than one character).

```ts
fillBefore("a", "b", 2); // result: "ba"
fillBefore("a", "b", 4); // result: "bbba"
fillBefore("a", "bc", 4); // result: "bcbca"
```

### `fillAfter`

Fill a string with a _fill string_. The fill string is added **after** the base string until the total length is equal to the fill length (or possibly superior if the fill string has more than one character).

```ts
fillAfter("a", "b", 2); // result: "ab"
fillAfter("a", "b", 4); // result: "abbb"
fillAfter("a", "bc", 4); // result: "abcbc"
```

### `fill0`

This function is a kind of alias for a `fillBefore` with `0`. Since it mostly used with number, it can take a number or a string as base.

```ts
fill0("1", 2); // result: "01"
fill0(1, 2); // result: "01"
fill0(99, 3); // result: "099"
```

### `firstNotNull`

Returns the first not null element of an array.

```ts
const a = // any object
const b = // any other object
firstNotNull([null, a, b]); // result: a
```

### `lastNotNull`

Returns the last not null element of an array.

```ts
const a = // any object
const b = // any other object
lastNotNull([a, b, null]); // result: b
```

### `genNumericalCode`

Generate a random numerical code as string (default with 4 digits)

```ts
genNumericalCode(); // result (example): "5604"
genNumericalCode(2); // result (example): "39"
```

### `keys`

Returns keys of an object.

```ts
keys({ a: "toto", b: 25 }); // result: ["a", "b"]
```

### `values`

Returns values of an object.

```ts
values({ a: "toto", b: 25 }); // result: ["toto", 25]
```

### `each`

Apply callback function on each item of iterable or each key of item

```ts
each([1, 2, 3], (v, k) => value + 1); // result: [2,3,4]
each({ a: 1, b: 2 }, (v, k) => `${k}${v}`); // result: {a:"a1", b:"b2"}
```

### `some`

Return true if some items of an object validate callback function (condition)

```ts
function isValid(value, key) {
  return value > 10;
}

some({ a: 1, b: 25, c: 12 }, isValid); // return: true
some({ a: 1, b: 2 }, isValid); // return: false
```

### `every`

Return true if every items of an object valiade callback function (conditon)

```ts
function isValid(value, key) {
  return value > 10;
}

every({ a: 11, b: 25, c: 12 }, isValid); // return: true
every({ a: 1, b: 25, c: 12 }, isValid); // return: false (a > 10 is false)
```

### `extend`

Allow to extend an object with another objects

```ts
extend({ a: 1 }, { b: 2 }, { c: 3 }); // result: {a:1, b:2, c:3}
```

### `omit`

Copy an object without some keys

```ts
omit({ a: 1, b: 2, c: 3 }, "b"); // result: {a:1, c:3}
```

### `pick`

Copy an object with only selected keys

```ts
pick({ a: 1, b: 2, c: 3 }, "a", "b"); // result: {a:1, b:2}
```

### `isMatch`

Return true any keys-value of item are in obj

```ts
isMatch({ a: 1, b: 2, c: 3 }, { a: 1, b: 2 }); // result: true
isMatch({ a: 1, c: 3 }, { a: 1, b: 2 }); // result: false (b missing)
isMatch({ a: 1, b: 2, c: 3 }, { a: 1, b: 3 }); // result: false (b is different)
```

### `isEmpty`

Return true if an object has no keys

```ts
isEmpty({}); // result: true
isEmpty({ a: 1 }); // result: false
```

### `merge`

Recursively merge the source object into the target object

### `shuffle`

Suffle an array of data. Based on the Fisher–Yates algorithm.

```ts
shuffle([1, 2, 3, 4]); // result (example): [3,1,2,4]
```

### `value2str`

Convert a value to string (with string header to identify type).

```ts
value2str(25); // result: "number:25"
value2str(true); // result: "boolean:true"
value2str("test"); // result: "string:test"
value2str("25"); // result: "string:25"
```

### `str2value`

Convert a string from `value2str` to the correctly typed value

```ts
str2value("number:25"); // result: 25
str2value("boolean:true"); // result: true
str2value("string:test"); // result: "test"
str2value("string:25"); // result "25"
```

### `diffDate`

Get the `getTime` difference between two dates.

### `isFirstNewer`

Test if the first date parameter is newer.

```ts
const oldDate = new Date(...);  // older date
const newDate = new Date(...);  // newer date

isFirstNewer(newDate, oldDate); // result: true
isFirstNewer(oldDate, newDate); // result: false
```

### `isFirstOlder`

Test if the first date parameter is older.

```ts
const oldDate = new Date(...);  // older date
const newDate = new Date(...);  // newer date

isFirstNewer(newDate, oldDate); // result: false
isFirstNewer(oldDate, newDate); // result: true
```

### `isSameDate`

Test if two date are equals.

```ts
const a = new Date("2021-05-01");
const b = new Date("2021-05-02");
const c = new Date("2021-05-01");

isSameDate(a, b); // result: false (not the same day)
isSameDate(a, c); // result: true
```

### `sortOlderFirst`

Function to use in sort to order by older first.

### `sortNewerFirst`

Function to use in sort to order by newer first.

### `removeTime`

Set time part of a `Date` object to `0`.

### `upperFirstLetter`

```ts
upperFirstLetter("hello"); // result: "Hello"
```

### `urlify`

Prefix URL with `http://`

```ts
urlify("www.test.com"); // result: "http://www.test.com"
urlify("http://www.test.com"); // result: "http://www.test.com"
urlify("https://www.test.com"); // result: "https://www.test.com"
```

### `isNumber`

Test if value is number.

```ts
isNumber(25); // result: true
isNumber(true); // result: false
```

### `isString`

Test if value is string.

```ts
isString("test"); // result: true
isString(25); // result: false
```

### `isBoolean`

Test if value is boolean.

```ts
isBoolean(false); // result: true
isBoolean(25); // result: false
```

### `isObject`

Test if value is object.

```ts
isObject({}); // result: true
isObject(null); // result: false
isObject(25); // result: false
isObject([]); // result: true
```

### `isStrictObject`

Test if value is object (but not array).

```ts
isStrictObject({}); // result: true
isStrictObject([]); // result: false
```

### `isBigInt`

### `isSymbol`

### `isFunction`

Test if value is a function.

```ts
isFunction(() => 2); // result: true
isFunction(25); // result: false
```

### `isArray`

Test if value is an array.

```ts
isArray([]); // result: true
isArray(25); // result: false
```

### `isUndefined`

Test if value is undefined.

```ts
isUndefined(undefined); // result: true
isUndefined(null); // result: false
isUndefined(25); // result: false
```

### `isNull`

Test if value is null.

```ts
isNull(null); // result: true
isNull(undefined); // result: false
isNull(25); // result: false
```

### `isPromise`

Test if value is a Promise.

### `exist`

Test if value exist (not undefined or null).

```ts
exist(25); // result: true
exist(false); // result: true
exist(undefined); // result: false
exist(null); // result: false
```

### `isValidEmail`

Check if string is a valid email format.

```ts
isValidEmail("a@b.c"); // result: true
isValidEmail("test"); // result: false
```

### `isValidUUID`

Check if string is a valid UUID format.

### `hasLowerCaseLetters`

Check if a string has lowercase.

```ts
hasLowerCaseLetters("AbC"); // result: true
hasLowerCaseLetters("ABC"); // result: false
```

### `hasUpperCaseLetters`

Check if a string has uppercase.

```ts
hasUpperCaseLetters("aBc"); // result: true
hasUpperCaseLetters("abc"); // result: false
```

### `hasNumbers`

Check if a string has number.

```ts
hasNumbers("abc2e"); // result: true
hasNumbers("abc"); // result: false
```

### `hasSpecialLetters`

Check if a string has special letters.

```ts
hasSpecialLetters("@bc"); // result: true
hasSpecialLetters("abc"); // result: false
```

### `isShortPassword`

Check if a password if too short (less than 8 characters).

```ts
isShortPassword("1234567"); // result: true
isShortPassword("12345678"); // result: false
```

### `isStrongPassword`

Check if a password is considered as strong. A strong password is a non short password with at least valid property from :

- has lower case
- has upper case
- has number
- has special character
