import { fillBefore, fillAfter, fill0 } from "../src";

describe("test fillBefore function", () => {
  it("fillable", () => {
    expect(fillBefore("a", "b", 2)).toBe("ba");
    expect(fillBefore("a", "b", 4)).toBe("bbba");
    expect(fillBefore("a", "bc", 4)).toBe("bcbca");
    expect(fillBefore("12345", "6", 6)).toBe("612345");
    expect(fillBefore("", "a", 2)).toBe("aa");
  });

  it("non fillable (exact number)", () => {
    expect(fillBefore("", "a", 0)).toBe("");
    expect(fillBefore("a", "b", 1)).toBe("a");
    expect(fillBefore("123456", "a", 6)).toBe("123456");
  });

  it("non fillable (longer base string)", () => {
    expect(fillBefore("1", "a", 0)).toBe("1");
    expect(fillBefore("123456", "ab", 5)).toBe("123456");
    expect(fillBefore("", "a", -1)).toBe("");
  });

  it("invalid fill string", () => {
    expect(fillBefore("a", "", 2)).toBe("a");
    expect(fillBefore("", "", 0)).toBe("");
    expect(fillBefore("abc", 3 as any, 2)).toBe("abc");
  });

  it("invalid base string", () => {
    expect(fillBefore(3 as any, "a", 2)).toBe("aa");
    expect(fillBefore(true as any, "a", 4)).toBe("aaaa");
    expect(fillBefore({} as any, "0", 3)).toBe("000");
  });

  it("invalid base string & fill string", () => {
    expect(fillBefore(3 as any, true as any, 2)).toBe("");
  });

  it("handle default values", () => {
    expect(fillBefore("a")).toBe(" a");
    expect(fillBefore("")).toBe("  ");
    expect(fillBefore(3 as any)).toBe("  ");
  });
});

describe("test fillAfter function", () => {
  it("fillable", () => {
    expect(fillAfter("a", "b", 2)).toBe("ab");
    expect(fillAfter("a", "b", 4)).toBe("abbb");
    expect(fillAfter("a", "bc", 4)).toBe("abcbc");
    expect(fillAfter("12345", "6", 6)).toBe("123456");
    expect(fillAfter("", "a", 2)).toBe("aa");
  });

  it("non fillable (exact number)", () => {
    expect(fillAfter("", "a", 0)).toBe("");
    expect(fillAfter("a", "b", 1)).toBe("a");
    expect(fillAfter("123456", "a", 6)).toBe("123456");
  });

  it("non fillable (longer base string)", () => {
    expect(fillAfter("1", "a", 0)).toBe("1");
    expect(fillAfter("123456", "ab", 5)).toBe("123456");
    expect(fillAfter("", "a", -1)).toBe("");
  });

  it("non fillable (invalid fill string)", () => {
    expect(fillAfter("a", "", 2)).toBe("a");
    expect(fillAfter("", "", 0)).toBe("");
    expect(fillAfter("abc", 3 as any, 2)).toBe("abc");
  });

  it("invalid base string", () => {
    expect(fillBefore(3 as any, "a", 2)).toBe("aa");
    expect(fillBefore(true as any, "a", 4)).toBe("aaaa");
    expect(fillBefore({} as any, "0", 3)).toBe("000");
  });

  it("invalid base string & fill string", () => {
    expect(fillAfter(3 as any, true as any, 2)).toBe("");
  });

  it("handle default values", () => {
    expect(fillAfter("a")).toBe("a ");
    expect(fillAfter("")).toBe("  ");
    expect(fillAfter(3 as any)).toBe("  ");
  });
});

describe("test fill0 function", () => {
  it("fillable", () => {
    expect(fill0("", 1)).toBe("0");
    expect(fill0("1", 2)).toBe("01");
    expect(fill0("1", 4)).toBe("0001");
    expect(fill0("99", 3)).toBe("099");
    expect(fill0(5, 2)).toBe("05");
  });

  it("non fillable (exact number)", () => {
    expect(fill0("", 0)).toBe("");
    expect(fill0("1", 1)).toBe("1");
    expect(fill0("56", 2)).toBe("56");
    expect(fill0("1000", 4)).toBe("1000");
    expect(fill0(12, 2)).toBe("12");
  });

  it("non fillable (longer base string)", () => {
    expect(fill0("1", 0)).toBe("1");
    expect(fill0("1", -1)).toBe("1");
    expect(fill0("18", 1)).toBe("18");
    expect(fill0("123456", 4)).toBe("123456");
    expect(fill0(123, 2)).toBe("123");
  });

  it("handle default values", () => {
    expect(fill0(1)).toBe("01");
    expect(fill0("")).toBe("00");
  });
});
