import { wait, runParallel, applyParallel } from "../src";

async function double(x: number) {
  await wait(100);
  return x * 2;
}

describe("run parallel", () => {
  it("should run in parallel", async () => {
    const results = await runParallel(double(1), double(2), double(3));
    expect(results).toEqual([2, 4, 6]);
  });

  it("should apply in parallel", async () => {
    const results = await applyParallel([1, 2, 3], double);
    expect(results).toEqual([2, 4, 6]);
  });
});
