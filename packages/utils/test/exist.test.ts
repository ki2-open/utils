import { exist, countExist, existAll, existSome } from "../src";

describe("test exist function", () => {
  it("should valid existing boolean", () => {
    expect(exist(true)).toBe(true);
    expect(exist(false)).toBe(true);
  });

  it("should valid existing number", () => {
    expect(exist(15)).toBe(true);
    expect(exist(0)).toBe(true);
    expect(exist(-42)).toBe(true);
  });

  it("should valid existing string", () => {
    expect(exist("test")).toBe(true);
    expect(exist("")).toBe(true);
  });

  it("should valid existing object", () => {
    expect(exist({ a: "b" })).toBe(true);
    expect(exist({})).toBe(true);
  });

  it("should valid existing array", () => {
    expect(exist([1, 2, 3])).toBe(true);
    expect(exist([])).toBe(true);
  });

  it("should valid existing bigint", () => {
    expect(exist(BigInt(0))).toBe(true);
  });

  it("should not valid null item", () => {
    expect(exist(null)).toBe(false);
  });

  it("should not valid undefined item", () => {
    expect(exist(undefined)).toBe(false);
  });

  it("should validate count exist", () => {
    expect(countExist([1, 2, 3])).toBe(3);
    expect(countExist([1, null, undefined])).toBe(1);
    expect(countExist(["a"])).toBe(1);
    expect(countExist([null, null])).toBe(0);
    expect(countExist([undefined])).toBe(0);
    expect(countExist([undefined, true])).toBe(1);

    expect(countExist([])).toBe(0);
  });

  it("should validate existAll", () => {
    expect(existAll([1, 2, 3])).toBe(true);
    expect(existAll([1, null])).toBe(false);
    expect(existAll(["a"])).toBe(true);
    expect(existAll([undefined, undefined])).toBe(false);
    expect(existAll([null])).toBe(false);
    expect(existAll([undefined])).toBe(false);

    expect(existAll([])).toBe(true);
  });

  it("should validate existSome", () => {
    expect(existSome([1, 2, 3])).toBe(true);
    expect(existSome([1, null])).toBe(true);
    expect(existSome(["a"])).toBe(true);
    expect(existSome([undefined, undefined])).toBe(false);
    expect(existSome([null])).toBe(false);
    expect(existSome([undefined])).toBe(false);

    expect(existSome([])).toBe(false);
  });
});
