import {
  isArray,
  isBigInt,
  isBoolean,
  isFunction,
  isNumber,
  isObject,
  isStrictObject,
  isString,
  isSymbol,
  isUndefined,
  isNull,
  isPromise,
  isStrictNaN,
  isError,
} from "../src";

describe("test base types checks", () => {
  it("valid bigint", () => {
    expect(isBigInt(BigInt(25))).toBe(true);
  });

  it("invalid bigint", () => {
    expect(isBigInt("")).toBe(false);
  });

  it("valid boolean", () => {
    expect(isBoolean(true)).toBe(true);
    expect(isBoolean(false)).toBe(true);
  });

  it("invalid boolean", () => {
    expect(isBoolean("")).toBe(false);
    expect(isBoolean(0)).toBe(false);
  });

  it("valid function", () => {
    expect(isFunction(() => true)).toBe(true);
  });

  it("invalid function", () => {
    expect(isFunction(true)).toBe(false);
  });

  it("valid number", () => {
    expect(isNumber(0)).toBe(true);
    expect(isNumber(75)).toBe(true);
    expect(isNumber(-75)).toBe(true);
    expect(isNumber(Number(2))).toBe(true);
  });

  it("invalid number", () => {
    expect(isNumber(true)).toBe(false);
    expect(isNumber(BigInt(12))).toBe(false);
    expect(isNumber("12")).toBe(false);
  });

  it("valid object", () => {
    expect(isObject({ a: true })).toBe(true);
    expect(isObject({})).toBe(true);
    expect(isObject([])).toBe(true);
  });

  it("invalid object", () => {
    expect(isObject(true)).toBe(false);
    expect(isObject("")).toBe(false);
    expect(isObject(25)).toBe(false);
    expect(isObject(null)).toBe(false);
    expect(isObject(undefined)).toBe(false);
  });

  it("valid strict object", () => {
    expect(isStrictObject({ a: true })).toBe(true);
    expect(isStrictObject({})).toBe(true);
  });

  it("invalid strict object", () => {
    expect(isStrictObject(true)).toBe(false);
    expect(isStrictObject("")).toBe(false);
    expect(isStrictObject(25)).toBe(false);
    expect(isStrictObject(null)).toBe(false);
    expect(isStrictObject(undefined)).toBe(false);
    expect(isStrictObject([])).toBe(false);
  });

  it("valid string", () => {
    expect(isString("")).toBe(true);
    expect(isString("aaa")).toBe(true);
  });

  it("invalid string", () => {
    expect(isString(true)).toBe(false);
    expect(isString(0)).toBe(false);
  });

  it("valid symbol", () => {
    expect(isSymbol(Symbol())).toBe(true);
    expect(isSymbol(Symbol(0))).toBe(true);
    expect(isSymbol(Symbol(""))).toBe(true);
  });

  it("invalid symbol", () => {
    expect(isSymbol(false)).toBe(false);
    expect(isSymbol(0)).toBe(false);
  });

  it("valid Array", () => {
    expect(isArray([])).toBe(true);
    expect(isArray([1, 2, 3])).toBe(true);
    expect(isArray([null])).toBe(true);
    expect(isArray(["a", "b"])).toBe(true);
  });

  it("invalid Array", () => {
    expect(isArray(undefined)).toBe(false);
    expect(isArray(null)).toBe(false);
    expect(isArray("abc")).toBe(false);
    expect(isArray(123)).toBe(false);
    expect(isArray({})).toBe(false);
  });

  it("valid undefined", () => {
    expect(isUndefined(undefined)).toBe(true);
  });

  it("invalid undefined", () => {
    expect(isUndefined(null)).toBe(false);
    expect(isUndefined(false)).toBe(false);
    expect(isUndefined("")).toBe(false);
    expect(isUndefined(0)).toBe(false);
  });

  it("valid null", () => {
    expect(isNull(null)).toBe(true);
  });

  it("invalid null", () => {
    expect(isNull(undefined)).toBe(false);
    expect(isNull(false)).toBe(false);
    expect(isNull("")).toBe(false);
    expect(isNull(0)).toBe(false);
  });

  it("valid promise", () => {
    class SubError extends Error {
      constructor(message: string) {
        super(message);
      }

      get mmm() {
        return this.message.toUpperCase();
      }
    }
    expect(isPromise({ then: () => 0 })).toBe(true);

    const p = new Promise((resolve, reject) => {
      resolve(true);
    });
    expect(isPromise(p)).toBe(true);
  });

  it("invalid promise", () => {
    expect(isPromise(0)).toBe(false);
    expect(isPromise({})).toBe(false);
  });

  it("validate root isNaN behavior", () => {
    /**
     * The parameter of isNaN function is converted to number before check.
     * Thus isNaN return true if Number(value) is NaN.
     */

    // non NaN number to not be NaN
    expect(isNaN(0)).toBe(false);
    expect(isNaN(42)).toBe(false);
    expect(isNaN(-42)).toBe(false);
    expect(isNaN(Infinity)).toBe(false);

    // null is not NaN but undefined is considered as NaN
    expect(isNaN(null as any)).toBe(false);
    expect(isNaN(undefined as any)).toBe(true);

    // string that can be interpreted as number are not NaN (other are considered NaN)
    expect(isNaN("" as any)).toBe(false);
    expect(isNaN("12" as any)).toBe(false);
    expect(isNaN("toto" as any)).toBe(true);

    // Non numbers are NaN
    expect(isNaN((() => 1) as any)).toBe(true);
    expect(isNaN({} as any)).toBe(true);

    expect(isNaN(NaN)).toBe(true);
  });

  it("validate isStrictNaN behavior", () => {
    expect(isStrictNaN(0)).toBe(false);
    expect(isStrictNaN(42)).toBe(false);
    expect(isStrictNaN(-42)).toBe(false);
    expect(isStrictNaN(Infinity)).toBe(false);

    expect(isStrictNaN(null as any)).toBe(false);
    expect(isStrictNaN(undefined as any)).toBe(false);

    expect(isStrictNaN("" as any)).toBe(false);
    expect(isStrictNaN("12" as any)).toBe(false);
    expect(isStrictNaN("toto" as any)).toBe(false);

    expect(isStrictNaN((() => 1) as any)).toBe(false);
    expect(isStrictNaN({} as any)).toBe(false);

    expect(isStrictNaN(NaN)).toBe(true);
  });

  it("validate isError behavior", () => {
    expect(isError(new Error())).toBe(true);
    expect(isError(2)).toBe(false);

    class SubClass extends Error {}
    expect(isError(new SubClass())).toBe(true);
  });
});
