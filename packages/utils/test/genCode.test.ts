import { genNumericalCode } from "../src";

describe("test genNumericalCode function", () => {
  it("valid output type", () => {
    expect(typeof genNumericalCode()).toBe("string");
    expect(typeof genNumericalCode(5)).toBe("string");
    expect(typeof genNumericalCode(-1)).toBe("string");
  });

  it("valid default length", () => {
    expect(genNumericalCode().length).toBe(4);
  });

  it("valid custom length", () => {
    expect(genNumericalCode(1).length).toBe(1);
    expect(genNumericalCode(8).length).toBe(8);
  });

  it("valid zero or negatives values", () => {
    expect(genNumericalCode(0).length).toBe(1);
    expect(genNumericalCode(-1).length).toBe(1);
  });
});
