import {
  firstExist,
  lastExist,
  filterExist,
  filterNull,
  filterUndefined,
  filterObject,
} from "../src";

describe("test firstNotNull", () => {
  it("should return valid item", () => {
    expect(firstExist([0, 1, 2, 3])).toBe(0);
    expect(firstExist([0])).toBe(0);
    expect(firstExist([null, 1])).toBe(1);
    expect(firstExist([null, null, null, 1])).toBe(1);
    expect(firstExist([null, undefined, undefined, 1])).toBe(1);
  });

  it("should return null item", () => {
    expect(firstExist([null, null, null])).toBe(null);
    expect(firstExist([undefined, undefined])).toBe(null);
    expect(firstExist([])).toBe(null);
    expect(firstExist([null])).toBe(null);
    expect(firstExist([undefined])).toBe(null);
  });
});

describe("test lastNotNull", () => {
  it("should return valid item", () => {
    expect(lastExist([0, 1, 2, 3])).toBe(3);
    expect(lastExist([0])).toBe(0);
    expect(lastExist([1, null])).toBe(1);
    expect(lastExist([1, null, null, null])).toBe(1);
    expect(lastExist([1, undefined, undefined, null])).toBe(1);
  });

  it("should return null item", () => {
    expect(lastExist([null, null, null])).toBe(null);
    expect(lastExist([undefined, undefined])).toBe(null);
    expect(lastExist([])).toBe(null);
    expect(lastExist([undefined])).toBe(null);
  });
});

describe("test array null/undefined filters", () => {
  it("should extract exiting items (not null/undefined)", () => {
    expect(filterExist([0, 1, 2, 3])).toEqual([0, 1, 2, 3]);
    expect(filterExist([1, null, 2, undefined, 3])).toEqual([1, 2, 3]);
    expect(filterExist([])).toEqual([]);
    expect(filterExist([null])).toEqual([]);
    expect(filterExist([undefined])).toEqual([]);
  });

  it("should extract non undefined items", () => {
    expect(filterUndefined([0, 1, 2, 3])).toEqual([0, 1, 2, 3]);
    expect(filterUndefined([1, null, 2, undefined, 3])).toEqual([
      1,
      null,
      2,
      3,
    ]);
    expect(filterUndefined([])).toEqual([]);
    expect(filterUndefined([null])).toEqual([null]);
    expect(filterUndefined([undefined])).toEqual([]);
  });

  it("should extract non null items", () => {
    expect(filterNull([0, 1, 2, 3])).toEqual([0, 1, 2, 3]);
    expect(filterNull([1, null, 2, undefined, 3])).toEqual([
      1,
      2,
      undefined,
      3,
    ]);
    expect(filterNull([])).toEqual([]);
    expect(filterNull([null])).toEqual([]);
    expect(filterNull([undefined])).toEqual([undefined]);
  });
});

describe("test filter object", () => {
  it("should keep existing properties", () => {
    const obj = { a: 1, b: 2, c: 3 };
    const fobj = filterObject(obj);
    expect(fobj).toEqual(obj);
    expect("a" in fobj).toBe(true);
    expect("b" in fobj).toBe(true);
    expect("c" in fobj).toBe(true);
    expect("d" in fobj).toBe(false);
  });

  it("should remove undefined properties", () => {
    const obj = { a: 1, b: undefined, c: 3 };
    const fobj = filterObject(obj);
    expect(filterObject(fobj)).toEqual({ a: 1, c: 3 });
    expect("a" in fobj).toBe(true);
    expect("b" in fobj).toBe(false);
    expect("c" in fobj).toBe(true);
    expect("d" in fobj).toBe(false);
  });
});
