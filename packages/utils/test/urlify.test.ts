import { urlify } from "../src";

describe("test urlify function", () => {
  it("test function", () => {
    expect(urlify("toto")).toBe("http://toto");
    expect(urlify("http://toto")).toBe("http://toto");
    expect(urlify("https://toto")).toBe("https://toto");
  });
});
