import { keys, values, extend, omit, pick, isMatch, merge } from "../src";

describe("test lodashy functions", () => {
  it("should validate keys", () => {
    expect(keys({ a: 1, b: 2 })).toEqual(["a", "b"]);
    expect(keys(["a", "b"])).toEqual(["0", "1"]);
    expect(keys("abc" as any)).toEqual(["0", "1", "2"]);

    expect(keys(25 as any)).toEqual([]);
    expect(keys(() => null)).toEqual([]);
    expect(keys(Symbol("toto") as any)).toEqual([]);
    expect(keys(true as any)).toEqual([]);

    expect(() => keys(null as any)).toThrow(TypeError);
    expect(() => keys(undefined as any)).toThrow(TypeError);
  });

  it("should validate values", () => {
    expect(values({ a: 1, b: 2 })).toEqual([1, 2]);
    expect(values(["a", "b"])).toEqual(["a", "b"]);
    expect(values("abc")).toEqual(["a", "b", "c"]);
    expect(values({ a: 1, b: "toto" })).toEqual([1, "toto"]);

    expect(values(25)).toEqual([]);
    expect(values(() => null)).toEqual([]);
    expect(values(Symbol("toto"))).toEqual([]);
    expect(values(true)).toEqual([]);

    expect(() => values(null)).toThrow(TypeError);
    expect(() => values(undefined)).toThrow(TypeError);
  });

  it("should validate extend", () => {
    expect(extend({}, { a: 1 })).toEqual({ a: 1 });
    expect(extend({ a: 1 }, { b: 2 }, { c: 3 })).toEqual({ a: 1, b: 2, c: 3 });
    expect(extend({ a: 1, b: 2 }, { b: 3 })).toEqual({ a: 1, b: 3 });

    expect(extend({}, false as any)).toEqual({});
    expect(extend({}, 25 as any)).toEqual({});
    expect(extend({ a: 1 }, 12 as any)).toEqual({ a: 1 });

    expect(extend({ a: 1 }, null as any)).toEqual({ a: 1 });
    expect(extend({ a: 1 }, [2])).toEqual({ a: 1, "0": 2 });
  });

  it("should validate omit", () => {
    expect(omit({ a: 1, b: 2 }, "a")).toEqual({ b: 2 });
    expect(omit({ a: 1, b: 2, c: 3 }, "b", "a")).toEqual({ c: 3 });

    expect(omit({ a: 1 }, "a")).toEqual({});
    expect(omit({ a: 1, b: "toto" }, "a", "b")).toEqual({});

    expect(omit({ a: 1 }, "c" as any)).toEqual({ a: 1 });
    expect(omit({ a: 1 })).toEqual({ a: 1 });
  });

  it("should validate pick", () => {
    expect(pick({ a: 1, b: 2 }, "b")).toEqual({ b: 2 });
    expect(pick({ a: 1, b: 2, c: 3 }, "a", "c")).toEqual({ a: 1, c: 3 });

    expect(pick({ a: 1, b: 2 }, "c" as any)).toEqual({});
    expect(pick({ a: 1, b: 2 })).toEqual({});
  });

  it("should validate isMatch", () => {
    expect(isMatch({ a: 1 }, { a: 1 })).toBe(true);
    expect(isMatch({ a: 1, b: 2 }, { a: 1, b: 2 })).toBe(true);
    expect(isMatch({ a: 1, b: 2 }, { a: 1 })).toBe(true);

    expect(isMatch({ a: 1 }, { b: 2 })).toBe(false);
    expect(isMatch({ a: 1 }, { a: 1, b: 2 })).toBe(false);
  });

  it("should validate merge", () => {
    expect(merge({ a: { b: 1 } }, { a: { c: 2 }, b: 3 })).toEqual({
      a: {
        b: 1,
        c: 2,
      },
      b: 3,
    });
  });
});
