import { value2str, str2value } from "../src";

describe("test strvalues functions", () => {
  it("convert value to string", () => {
    expect(value2str(25)).toBe("number:25");
    expect(value2str(-12)).toBe("number:-12");
    expect(value2str(0)).toBe("number:0");
    expect(value2str(true)).toBe("boolean:true");
    expect(value2str(false)).toBe("boolean:false");
    expect(value2str("hello")).toBe("string:hello");
  });

  it("convert string to value", () => {
    expect(str2value("number:25")).toBe(25);
    expect(str2value("number:-12")).toBe(-12);
    expect(str2value("number:0")).toBe(0);
    expect(str2value("boolean:true")).toBe(true);
    expect(str2value("boolean:false")).toBe(false);
    expect(str2value("boolean:aaa")).toBe(false);
    expect(str2value("string:hello")).toBe("hello");
  });
});
