import { isEmpty, hasKey, hasKeys, hasSymbolKey } from "../src";

describe("test object functions", () => {
  it("should validate isEmpty", () => {
    expect(isEmpty({})).toBe(true);
    expect(isEmpty([])).toBe(true);

    expect(isEmpty({ a: 1 })).toBe(false);
    expect(isEmpty([1])).toBe(false);
  });

  it("should validate hasSymbolKey", () => {
    const sym = Symbol("a");
    let o = { [sym]: 1 };
    expect(hasSymbolKey(o, sym)).toBe(true);
    expect(hasSymbolKey(o, Symbol("a"))).toBe(false);
  });

  it("should validate hasKey", () => {
    expect(hasKey({}, "a")).toBe(false);
    expect(hasKey({ a: 1 }, "A")).toBe(false);
    expect(hasKey([], 2)).toBe(false);
    expect(hasKey({}, 0)).toBe(false);
    expect(hasKey({}, Symbol("symbol"))).toBe(false);

    expect(hasKey({ a: 1 }, "a")).toBe(true);
    expect(hasKey({ a: 1, b: 2 }, "a")).toBe(true);
    expect(hasKey(["a"], 0)).toBe(true);
    const sym = Symbol("b");
    let a = { [sym]: 42 };
    expect(hasKey(a, sym)).toBe(true);
  });

  it("should validate hasKeys", () => {
    expect(hasKeys({}, "a")).toBe(false);
    expect(hasKeys({ a: 1 }, "a", "b")).toBe(false);
    expect(hasKeys(["a"], 0, 1)).toBe(false);

    expect(hasKeys({ a: 1 }, "a")).toBe(true);
    expect(hasKeys({ a: 1, b: 2 }, "a", "b")).toBe(true);
    expect(hasKeys({ a: 1, b: 2, c: 3 }, "a", "b")).toBe(true);
    expect(hasKeys(["a", "b"], 0, 1)).toBe(true);
    expect(hasKeys(["a", "b", "c"], 0, 1)).toBe(true);
  });
});
