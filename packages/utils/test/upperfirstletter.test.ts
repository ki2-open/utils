import { upperFirstLetter } from "../src";

describe("test upper first letter function", () => {
  it("valid string", () => {
    expect(upperFirstLetter("toto")).toBe("Toto");
    expect(upperFirstLetter("Toto")).toBe("Toto");
    expect(upperFirstLetter("t")).toBe("T");
  });

  it("empty data", () => {
    expect(upperFirstLetter(undefined)).toBe(undefined);
    expect(upperFirstLetter("")).toBe("");
  });
});
