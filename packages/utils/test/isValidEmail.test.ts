import { isValidEmail } from "../src";

describe("test isValidEmail function", () => {
  it("should be valid email", () => {
    expect(isValidEmail("a@b.c")).toBe(true);
    expect(isValidEmail("a.b.c.d.e@a.b.c.d.e")).toBe(true);
    expect(isValidEmail("abc.def@aaa.com")).toBe(true);
    expect(isValidEmail("a_b@test_.com")).toBe(true);
    expect(isValidEmail("_@test.com")).toBe(true);
    expect(isValidEmail("a-b@test.com")).toBe(true);
    expect(isValidEmail("a+b@test.com")).toBe(true);
    expect(isValidEmail("a.b+123@test.com")).toBe(true);
  });

  it("should be invalid email", () => {
    expect(isValidEmail("")).toBe(false);
    expect(isValidEmail("@")).toBe(false);
    expect(isValidEmail("a@")).toBe(false);
    expect(isValidEmail("a.b@")).toBe(false);
    expect(isValidEmail("@a")).toBe(false);
    expect(isValidEmail("@a.b")).toBe(false);
    expect(isValidEmail("a@b")).toBe(false);
    expect(isValidEmail(".a@b.c")).toBe(false);
    expect(isValidEmail("a@b.")).toBe(false);
    expect(isValidEmail("a@.c")).toBe(false);
    expect(isValidEmail("+a@b.c")).toBe(false);
    expect(isValidEmail("a.+a@b.c")).toBe(false);
  });
});
