import { isValidUUID } from "../src";
import { v4 as uuid4 } from "uuid";

describe("test isValidUUID function", () => {
  it("should be valid uuid", () => {
    expect(isValidUUID(uuid4())).toBe(true);
  });

  it("shouldn't be valid uuid", () => {
    expect(isValidUUID("")).toBe(false);
    expect(isValidUUID("123")).toBe(false);
    expect(isValidUUID(123)).toBe(false);
  });
});
