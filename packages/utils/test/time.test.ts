import {
  diffDate,
  isFirstNewer,
  isFirstOlder,
  isSameDate,
  sortOlderFirst,
  sortNewerFirst,
  removeTime,
} from "../src";

describe("test time function helpers", () => {
  const a = new Date("2012-04-06T16:56:02");
  const b = new Date(a);
  const c = new Date(a);
  b.setMilliseconds(b.getMilliseconds() + 10);

  it("valid diff date", () => {
    expect(diffDate(b, a)).toBe(10);
  });

  it("test first newer", () => {
    expect(isFirstNewer(a, b)).toBe(false);
    expect(isFirstNewer(b, a)).toBe(true);
    expect(isFirstNewer(a, c)).toBe(false);
  });

  it("test first older", () => {
    expect(isFirstOlder(a, b)).toBe(true);
    expect(isFirstOlder(b, a)).toBe(false);
    expect(isFirstOlder(a, c)).toBe(false);
  });

  it("test same date", () => {
    expect(isSameDate(a, b)).toBe(false);
    expect(isSameDate(b, a)).toBe(false);
    expect(isSameDate(a, c)).toBe(true);
  });

  it("test sort older first", () => {
    expect(sortOlderFirst(null, null)).toBe(0);
    expect(sortOlderFirst(a, c)).toBe(0);
    expect(sortOlderFirst(null, a)).toBe(1);
    expect(sortOlderFirst(a, null)).toBe(-1);
    expect(sortOlderFirst(a, b)).toBeLessThan(0);
    expect(sortOlderFirst(b, a)).toBeGreaterThan(0);
  });

  it("test sort older first", () => {
    expect(sortNewerFirst(null, null)).toBe(-0);
    expect(sortNewerFirst(a, c)).toBe(-0);
    expect(sortNewerFirst(null, a)).toBe(-1);
    expect(sortNewerFirst(a, null)).toBe(1);
    expect(sortNewerFirst(a, b)).toBeGreaterThan(0);
    expect(sortNewerFirst(b, a)).toBeLessThan(0);
  });

  it("test remove time", () => {
    const notime = removeTime(a);
    expect(notime.getMilliseconds()).toBe(0);
    expect(notime.getUTCSeconds()).toBe(0);
    expect(notime.getUTCMinutes()).toBe(0);
    expect(notime.getUTCHours()).toBe(0);
    expect(notime.getFullYear()).toBe(a.getFullYear());
    expect(notime.getMonth()).toBe(a.getMonth());
    expect(notime.getDate()).toBe(a.getDate());
  });
});
