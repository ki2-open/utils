import {
  hasLowerCaseLetters,
  hasUpperCaseLetters,
  hasNumbers,
  hasSpecialLetters,
  isShortPassword,
  isStrongPassword,
} from "../src";

describe("test passwords validation functions", () => {
  it("has lower case letters", () => {
    expect(hasLowerCaseLetters("a")).toBe(true);
    expect(hasLowerCaseLetters("aa")).toBe(true);
    expect(hasLowerCaseLetters("AtA")).toBe(true);
    expect(hasLowerCaseLetters("A")).toBe(false);
    expect(hasLowerCaseLetters("AAA")).toBe(false);
    expect(hasLowerCaseLetters("01654a")).toBe(true);
    expect(hasLowerCaseLetters("016540")).toBe(false);
    expect(hasLowerCaseLetters("")).toBe(false);
  });

  it("has upper case letters", () => {
    expect(hasUpperCaseLetters("A")).toBe(true);
    expect(hasUpperCaseLetters("AA")).toBe(true);
    expect(hasUpperCaseLetters("kBk")).toBe(true);
    expect(hasUpperCaseLetters("a")).toBe(false);
    expect(hasUpperCaseLetters("aaa")).toBe(false);
    expect(hasUpperCaseLetters("654654K")).toBe(true);
    expect(hasUpperCaseLetters("64654")).toBe(false);
    expect(hasUpperCaseLetters("")).toBe(false);
  });

  it("has number", () => {
    expect(hasNumbers("0")).toBe(true);
    expect(hasNumbers("99")).toBe(true);
    expect(hasNumbers("A0A")).toBe(true);
    expect(hasNumbers("a")).toBe(false);
    expect(hasNumbers("aaa")).toBe(false);
    expect(hasNumbers("sqdkljl4")).toBe(true);
    expect(hasNumbers("sklfjsdklj")).toBe(false);
    expect(hasNumbers("")).toBe(false);
  });

  it("has special chars", () => {
    expect(hasSpecialLetters("_")).toBe(true);
    expect(hasSpecialLetters("àç")).toBe(true);
    expect(hasSpecialLetters("AAAA_")).toBe(true);
    expect(hasSpecialLetters("a")).toBe(false);
    expect(hasSpecialLetters("5")).toBe(false);
    expect(hasSpecialLetters("aaa")).toBe(false);
    expect(hasSpecialLetters("sdlkfjsdlkfj#")).toBe(true);
    expect(hasSpecialLetters("sdfkljsdklj")).toBe(false);
    expect(hasSpecialLetters("")).toBe(false);
  });

  it("is short password", () => {
    expect(isShortPassword("12345678")).toBe(false);
    expect(isShortPassword("123456789")).toBe(false);
    expect(isShortPassword("1234567")).toBe(true);
    expect(isShortPassword("")).toBe(true);
  });

  it("is strong password", () => {
    expect(isStrongPassword("1234567")).toBe(false);
    expect(isStrongPassword("")).toBe(false);
    expect(isStrongPassword("12345678Aa_")).toBe(true);
    expect(isStrongPassword("12345678aa_")).toBe(true);
    expect(isStrongPassword("12345678AA_")).toBe(true);
    expect(isStrongPassword("12345678Aaa")).toBe(true);
    expect(isStrongPassword("aaaaaaaaaA_")).toBe(true);
    expect(isStrongPassword("123456789")).toBe(false);
    expect(isStrongPassword("aaaaaaaaa")).toBe(false);
    expect(isStrongPassword("_________")).toBe(false);
    expect(isStrongPassword("AAAAAAAAA")).toBe(false);
    expect(isStrongPassword("123456789_")).toBe(false);
    expect(isStrongPassword("123456789A")).toBe(false);
    expect(isStrongPassword("123456789a")).toBe(false);
    expect(isStrongPassword("aaaaaaaaaA")).toBe(false);
    expect(isStrongPassword("aaaaaaaaa_")).toBe(false);
    expect(isStrongPassword("AAAAAAAAA_")).toBe(false);
  });
});
