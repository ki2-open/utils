/**
 * Shuffles the elements of an array using the Fisher–Yates algorithm.
 *
 * The Fisher–Yates shuffle is an algorithm for generating a random permutation of a finite sequence.
 *
 * @param data The array to shuffle.
 * @returns A new array containing the elements of the input array shuffled randomly.
 * @link https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
 */
export function shuffle<T>(data: Array<T>): Array<T> {
  for (let i = data.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [data[i], data[j]] = [data[j], data[i]];
  }
  return data;
}

/**
 * Generates a random integer between 0 (inclusive) and the specified maximum value (exclusive).
 *
 * @param max The maximum value for the random integer (exclusive).
 * @returns A random integer between 0 (inclusive) and max (exclusive).
 */
export function randSimpleInt(max: number) {
  return Math.floor(Math.random() * max);
}

/**
 * Picks a random element from an array.
 *
 * @param arr The array from which to pick a random element.
 * @returns A random element from the input array.
 */
export function pickRandom<T = any>(arr: Array<T>): T {
  return arr[randSimpleInt(arr.length)];
}
