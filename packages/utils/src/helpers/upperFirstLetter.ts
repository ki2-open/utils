/**
 * Converts the first letter of a string to uppercase.
 *
 * If the input string is provided and is not empty, this function converts the first letter to uppercase and returns the modified string.
 * If the input string is empty or not provided, it returns the input string unchanged.
 *
 * @param s The input string.
 * @returns A new string with the first letter converted to uppercase, or the input string if it's empty or not provided.
 */
export function upperFirstLetter(s?: string) {
  if (typeof s === "string" && s.length > 0) {
    return s.charAt(0).toLocaleUpperCase() + s.slice(1);
  }
  return s;
}
