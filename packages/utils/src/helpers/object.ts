import { keys } from ".";
import { isSymbol } from "..";

export type ObjectKeyType = number | string | symbol;

/**
 * Checks if an object has no keys.
 *
 * @param obj The object to check.
 * @returns True if the object has no keys, otherwise false.
 */
export function isEmpty(obj: object) {
  return keys(obj).length === 0;
}

/**
 * Checks if an object has a specific symbol key.
 *
 * @param obj The object to check.
 * @param key The symbol key to check for.
 * @returns True if the object has the specified symbol key, otherwise false.
 */
export function hasSymbolKey(obj: object, key: symbol): boolean {
  return Object.getOwnPropertySymbols(obj).includes(key);
}

/**
 * Checks if an object has a specific key.
 *
 * @param obj The object to check.
 * @param key The key to check for.
 * @returns True if the object has the specified key, otherwise false.
 */
export function hasKey(obj: object, key: ObjectKeyType) {
  if (isSymbol(key)) {
    return hasSymbolKey(obj, key);
  }
  return key in obj;
}

/**
 * Checks if an object has all specified keys.
 *
 * @param obj The object to check.
 * @param keys The keys to check for.
 * @returns True if the object has all specified keys, otherwise false.
 */
export function hasKeys(obj: object, ...keys: ObjectKeyType[]) {
  for (const key of keys) {
    if (!hasKey(obj, key)) {
      return false;
    }
  }
  return true;
}
