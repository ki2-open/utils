import { isStrictObject } from "..";

export type KeyValueCallback<T> = (value: any, key: string) => T;

export interface StringKeyObject {
  [key: string]: any;
}

export type ExtendMultiple<T extends any[]> = T extends [infer R, ...infer Rest]
  ? R & ExtendMultiple<Rest>
  : unknown;

/**
 * Returns the keys of an object.
 *
 * @param obj The object from which to retrieve keys.
 * @returns An array containing the keys of the object.
 */
export function keys<T extends object>(obj: T): string[] {
  return Object.keys(obj);
}

/**
 * Returns the values of an object.
 *
 * @param obj The object from which to retrieve values.
 * @returns An array containing the values of the object.
 */
export function values(obj: number): never[];
export function values(obj: string): string[];
export function values<T>(obj: T[]): T[];
export function values<T extends object>(obj: T): Array<T[keyof T]>;
export function values(obj: any): string[];
export function values(obj: number | string | object): string[] {
  return Object.values(obj);
}

/**
 * Extends an object with properties from one or more other objects.
 *
 * @param first The first object to extend.
 * @param rest Additional objects whose properties will be merged into the first object.
 * @returns An object containing the merged properties from all provided objects.
 */
export function extend<T extends object, U extends object[]>(
  first: T,
  ...rest: U
): T & ExtendMultiple<U> {
  return Object.assign(first, ...rest);
}

/**
 * Creates a copy of an object excluding specified keys.
 *
 * @param obj The object from which to exclude keys.
 * @param keys The keys to exclude from the copied object.
 * @returns A new object containing all properties of the original object except those specified by keys.
 */
export function omit<T extends object, K extends keyof T>(
  obj: T,
  ...keys: K[]
): Omit<T, K> {
  const result = extend({}, obj);
  keys.forEach((key) => delete result[key]);
  return result;
}

/**
 * Creates a copy of an object including only specified keys.
 *
 * @param source The object from which to select keys.
 * @param keys The keys to include in the copied object.
 * @returns A new object containing only the properties specified by keys from the original object.
 */
export function pick<T extends object, K extends keyof T>(
  source: T,
  ...keys: K[]
): Pick<T, K> {
  return keys.reduce((result: any, key: K) => {
    if (key in source) {
      result[key] = source[key];
    }

    return result;
  }, {});
}

/**
 * Checks if any key-value pairs of the item object are present in the obj object.
 *
 * This function returns true if all key-value pairs of the item object are found in the obj object.
 *
 * @param obj The object to check against.
 * @param item The object whose key-value pairs are being searched for in obj.
 * @returns True if all key-value pairs of item are found in obj, otherwise false.
 */
export function isMatch(obj: any, item: any) {
  return keys(item).every((key) => obj[key] === item[key]);
}

/**
 * Recursively merges the source object into the target object.
 *
 * This function deeply merges the source object into the target object, combining their properties.
 *
 * @param target The object into which properties will be merged.
 * @param source The object from which properties will be merged into the target.
 * @returns A new object resulting from the merging of properties from both the target and source objects.
 */
export function merge<T extends StringKeyObject, U extends StringKeyObject>(
  target: T,
  source: U
): T & U {
  if (isStrictObject(target) && isStrictObject(source)) {
    keys(source).forEach((key) => {
      if (isStrictObject(source[key])) {
        if (!target[key]) {
          extend(target, { [key]: {} });
        }
        merge(target[key], source[key]);
      } else {
        extend(target, { [key]: source[key] });
      }
    });
  }
  return target as T & U;
}
