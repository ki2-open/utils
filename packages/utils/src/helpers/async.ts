/**
 * Asynchronously waits for a specified duration of time.
 *
 * @param duration The duration to wait for in milliseconds.
 * @returns A Promise that resolves after the specified duration.
 */
export function wait(duration: number): Promise<void> {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, duration);
  });
}

/**
 * Allows splitting long-running code by cooperatively releasing the execution context.
 *
 * This function is useful for breaking up heavy computation tasks to prevent blocking
 * the event loop. It can be used in both browser and Node.js environments.
 *
 * WARNING: While calling this function can reduce event-loop blocking, it may result
 * in significant performance reduction for poorly optimized code.
 */
export async function waitNextEventLoop() {
  await wait(0);
}
