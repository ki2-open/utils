/**
 * Converts a value to a string with a prefixed type identifier.
 *
 * This function takes a number, boolean, or string value and converts it to a string with a prefix indicating its type.
 * The prefix "number:" is added for numbers, "boolean:" for booleans, and "string:" for strings.
 *
 * @param value The value to convert to a string.
 * @returns A string representation of the value with a prefixed type identifier.
 */
export function value2str(value: number | boolean | string): string {
  let header = "string:";
  if (typeof value === "number") {
    header = "number:";
  } else if (typeof value === "boolean") {
    header = "boolean:";
  }

  return header + String(value);
}

/**
 * Converts a string with a prefixed type identifier back to its original value type.
 *
 * This function takes a string with a prefixed type identifier and converts it back to its original value type.
 * The prefix "number:" is expected for numbers, "boolean:" for booleans, and "string:" for strings.
 *
 * @param str The string with a prefixed type identifier.
 * @returns The original value represented by the string.
 */
export function str2value(str: string): number | boolean | string {
  const arr = str.split(":");
  const header = arr.shift();
  const payload = arr.join(":");
  switch (header) {
    case "number":
      return parseFloat(payload);
    case "boolean":
      return payload === "true";
    default:
      return payload;
  }
}
