/**
 * Does nothing synchronously.
 *
 * This function is used to perform no operation and is synchronous.
 */
export function noop() {
  return;
}

/**
 * Does nothing asynchronously.
 *
 * This function is used to perform no operation but ensures an asynchronous call.
 * It returns a Promise that resolves immediately.
 */
export async function noop_async() {
  return new Promise<void>((resolve) => {
    resolve();
  });
}
