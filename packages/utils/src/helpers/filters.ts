import { exist, isNull, isUndefined } from "..";

/**
 * Filters out null and undefined values from an array.
 *
 * @param items An array of values possibly containing null or undefined.
 * @returns An array of values with null and undefined values filtered out.
 */
export function filterExist<T>(items: Array<T | null | undefined>): T[] {
  return items.filter(exist) as T[];
}

/**
 * Filters out undefined values from an array.
 *
 * @param items An array of values possibly containing undefined.
 * @returns An array of values with undefined values filtered out.
 */
export function filterUndefined<T>(item: Array<T | undefined>): T[] {
  return item.filter((item) => !isUndefined(item)) as T[];
}

/**
 * Filters out null values from an array.
 *
 * @param items An array of values possibly containing null.
 * @returns An array of values with null values filtered out.
 */
export function filterNull<T>(items: Array<T | null>): T[] {
  return items.filter((item) => !isNull(item)) as T[];
}

/**
 * Returns the first non-null and non-undefined value from an array.
 *
 * @param items An array of values.
 * @returns The first non-null and non-undefined value from the array, or null if none is found.
 */
export function firstExist<T>(items: Array<T | null | undefined>): T | null {
  for (const item of items) {
    if (exist(item)) {
      return item;
    }
  }
  return null;
}

/**
 * Returns the first non-null and non-undefined value from an array.
 *
 * @param items An array of values.
 * @returns The first non-null and non-undefined value from the array, or null if none is found.
 * @deprecated Use firstExist instead.
 */
export const firstNotNull = firstExist;

/**
 * Returns the last non-null and non-undefined value from an array.
 *
 * @param items An array of values.
 * @returns The last non-null and non-undefined value from the array, or null if none is found.
 */
export function lastExist<T>(items: Array<T | null | undefined>): T | null {
  for (let idx = items.length - 1; idx >= 0; idx--) {
    const item = items[idx];
    if (exist(item)) {
      return item;
    }
  }
  return null;
}

/**
 * Returns the last non-null and non-undefined value from an array.
 *
 * @param items An array of values.
 * @returns The last non-null and non-undefined value from the array, or null if none is found.
 * @deprecated Use lastExist instead
 */
export const lastNotNull = lastExist;

/**
 * Returns a new object with only the properties whose values are not null or undefined.
 *
 * @param obj The object to filter.
 * @returns A new object containing only the properties whose values are not null or undefined.
 */
export function filterObject<T extends object>(obj: T): T {
  return Object.fromEntries(
    Object.entries(obj).filter(([, value]) => exist(value))
  ) as T;
}
