/**
 * Runs multiple promises in parallel.
 *
 * This function takes multiple promises as arguments and returns a single promise that resolves to an array containing the resolved values of all input promises.
 *
 * @param args An array of promises to run in parallel.
 * @returns A promise that resolves to an array containing the resolved values of all input promises.
 */
export function runParallel<T extends readonly unknown[] | []>(
  ...args: T
): Promise<{ -readonly [P in keyof T]: Awaited<T[P]> }> {
  return Promise.all(args);
}

/**
 * Applies a function to each element of an array in parallel.
 *
 * This function applies the provided function to each element of the input array in parallel and returns a promise that resolves to an array containing the results of each function call.
 *
 * @param data An array of data to be processed.
 * @param func A function that processes each element of the input array and returns a promise.
 * @returns A promise that resolves to an array containing the results of applying the function to each element of the input array.
 */
export function applyParallel<T extends unknown, U extends unknown>(
  data: T[],
  func: (x: T) => Promise<U>
): Promise<Awaited<U>[]> {
  return Promise.all(data.map(func));
}
