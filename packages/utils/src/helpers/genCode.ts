/**
 * Generates a numerical code of the specified size.
 *
 * @param size The size of the numerical code to generate (default is 4).
 * @returns A string containing a numerical code of the specified size.
 */
export function genNumericalCode(size: number = 4): string {
  let code: string = "";
  if (size <= 0) {
    size = 1;
  }
  for (let i = 0; i < size; i++) {
    const num = Math.floor(Math.random() * 10);
    code += num.toString();
  }
  return code;
}
