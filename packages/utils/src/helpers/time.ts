import { exist } from "..";

/**
 * Calculates the difference in milliseconds between two dates.
 *
 * @param a The first date.
 * @param b The second date.
 * @returns The difference in milliseconds between a and b (a - b).
 */
export function diffDate(a: Date, b: Date) {
  return a.getTime() - b.getTime();
}

/**
 * Checks if the first date is newer than the second date.
 *
 * @param a The first date.
 * @param b The second date.
 * @returns True if the first date is newer than the second date, otherwise false.
 */
export function isFirstNewer(a: Date, b: Date) {
  return diffDate(a, b) > 0;
}

/**
 * Checks if the first date is older than the second date.
 *
 * @param a The first date.
 * @param b The second date.
 * @returns True if the first date is older than the second date, otherwise false.
 */
export function isFirstOlder(a: Date, b: Date) {
  return diffDate(a, b) < 0;
}

/**
 * Checks if two dates are the same.
 *
 * @param a The first date.
 * @param b The second date.
 * @returns True if both dates are the same, otherwise false.
 */
export function isSameDate(a: Date, b: Date) {
  return a.getTime() === b.getTime();
}

/**
 * Sorts dates in ascending order (older dates first).
 *
 * This function is intended for use with the sort function to sort dates in ascending order.
 *
 * @param a The first date.
 * @param b The second date.
 * @returns A negative value if a is older than b, a positive value if a is newer than b, or 0 if both dates are the same or null/undefined.
 */
export function sortOlderFirst(
  a: Date | undefined | null,
  b: Date | undefined | null
): number {
  if (a === b) {
    return 0;
  }

  if (!exist(a)) {
    return 1;
  }

  if (!exist(b)) {
    return -1;
  }

  return a.getTime() - b.getTime();
}

/**
 * Sorts dates in descending order (newer dates first).
 *
 * This function is intended for use with the sort function to sort dates in descending order.
 *
 * @param a The first date.
 * @param b The second date.
 * @returns A negative value if a is newer than b, a positive value if a is older than b, or 0 if both dates are the same or null/undefined.
 */
export function sortNewerFirst(
  a: Date | undefined | null,
  b: Date | undefined | null
) {
  return -sortOlderFirst(a, b);
}

/**
 * Removes the time component from a date.
 *
 * @param date The date from which to remove the time component.
 * @returns A new date with the time component set to 00:00:00.
 */
export function removeTime(date: Date): Date {
  const d = new Date(date);
  d.setMilliseconds(0);
  d.setSeconds(0);
  d.setHours(0);
  d.setMinutes(-d.getTimezoneOffset());
  return d;
}
