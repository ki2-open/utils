import { isString } from "..";

/**
 * Fills the beginning of a string with a specified filler string until it reaches the minimum length.
 *
 * @param value The string to be filled.
 * @param fillstr The filler string to prepend to the original string (default is a single space).
 * @param minLength The minimum length of the resulting string (default is 2).
 * @returns A string filled with the filler string at the beginning to meet the minimum length.
 */
export function fillBefore(
  value: string,
  fillstr: string = " ",
  minLengh: number = 2
) {
  let retstr = "";

  if (isString(value)) {
    retstr = value;
  }

  if (!isString(fillstr)) {
    return retstr;
  }

  if (fillstr.length === 0 || minLengh <= 0) {
    return retstr;
  }

  while (retstr.length < minLengh) {
    retstr = fillstr + retstr;
  }
  return retstr;
}

/**
 * Fills the end of a string with a specified filler string until it reaches the minimum length.
 *
 * @param value The string to be filled.
 * @param fillstr The filler string to append to the original string (default is a single space).
 * @param minLength The minimum length of the resulting string (default is 2).
 * @returns A string filled with the filler string at the end to meet the minimum length.
 */
export function fillAfter(
  value: string,
  fillstr: string = " ",
  minLengh: number = 2
) {
  let retstr = "";

  if (isString(value)) {
    retstr = value;
  }

  if (!isString(fillstr)) {
    return retstr;
  }

  if (fillstr.length === 0 || minLengh <= 0) {
    return retstr;
  }

  while (retstr.length < minLengh) {
    retstr += fillstr;
  }
  return retstr;
}

/**
 * Fills the beginning of a string representation of a number with zeros until it reaches the minimum length.
 *
 * If the provided value is not a string, it will be converted to a string before filling.
 *
 * @param value The value (string or number) to be filled with zeros.
 * @param minLength The minimum length of the resulting string (default is 2).
 * @returns A string representation of the value filled with zeros at the beginning to meet the minimum length.
 */
export function fill0(value: string | number, minLengh: number = 2) {
  return fillBefore(String(value), "0", minLengh);
}
