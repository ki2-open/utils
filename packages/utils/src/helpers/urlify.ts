/**
 * Ensures that a URL string is properly formatted with the appropriate scheme (http:// or https://).
 *
 * If the input URL already starts with "https://" or "http://", this function returns the input URL unchanged.
 * Otherwise, it prepends "http://" to the input URL.
 *
 * @param url The input URL string.
 * @returns A properly formatted URL string with the appropriate scheme (http:// or https://).
 */
export function urlify(url: string): string {
  if (url.startsWith("https://") || url.startsWith("http://")) {
    return url;
  }
  return `http://${url}`;
}
