export type Omit<T, K> = Pick<T, Exclude<keyof T, K>>;

export type Nullable<T> = T | null;
export type Disableable<T> = Nullable<T> | false;
export type Nonable<T> = T | "@none";
