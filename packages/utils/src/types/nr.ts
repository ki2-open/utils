export type NR<T> = T | undefined;
export type NRString = NR<string>;
export type NRNumber = NR<number>;
