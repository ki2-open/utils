/**
 * Checks if a given string represents a valid email address.
 *
 * @param email The string to validate as an email address.
 * @returns True if the string represents a valid email address, otherwise false.
 */
export function isValidEmail(email: string): boolean {
  const r =
    /^(?:[a-z0-9_-]+\.)*[a-z0-9_-]+(?:\+[a-z0-9_\.-]+)?@(?:[a-z0-9_-]+\.)+[a-z]+$/;
  return r.test(email.toLowerCase());
}
