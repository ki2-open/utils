/**
 * Checks if the value is a number.
 *
 * @param v The value to check.
 * @returns True if the value is a number, otherwise false.
 */
export function isNumber(v: unknown): v is number {
  return typeof v === "number";
}

/**
 * Checks if the value is a string.
 *
 * @param v The value to check.
 * @returns True if the value is a string, otherwise false.
 */
export function isString(v: unknown): v is string {
  return typeof v === "string";
}

/**
 * Checks if the value is a boolean.
 *
 * @param v The value to check.
 * @returns True if the value is a boolean, otherwise false.
 */
export function isBoolean(v: unknown): v is boolean {
  return typeof v === "boolean";
}

/**
 * Checks if the value is an object.
 *
 * @param v The value to check.
 * @returns True if the value is an object, otherwise false.
 */
export function isObject(v: unknown): v is any {
  return typeof v === "object" && v !== null;
}

/**
 * Checks if the value is a strict object (not an array).
 *
 * @param v The value to check.
 * @returns True if the value is a strict object (not an array), otherwise false.
 */
export function isStrictObject(v: unknown): v is any {
  return isObject(v) && !isArray(v);
}

/**
 * Checks if the value is a BigInt.
 *
 * @param v The value to check.
 * @returns True if the value is a BigInt, otherwise false.
 */
export function isBigInt(v: unknown): v is bigint {
  return typeof v === "bigint";
}

/**
 * Checks if the value is a symbol.
 *
 * @param v The value to check.
 * @returns True if the value is a symbol, otherwise false.
 */
export function isSymbol(v: unknown): v is symbol {
  return typeof v === "symbol";
}

/**
 * Checks if the value is a function.
 *
 * @param v The value to check.
 * @returns True if the value is a function, otherwise false.
 */
export function isFunction(v: unknown): boolean {
  return typeof v === "function";
}

/**
 * Checks if the value is an array.
 *
 * @param v The value to check.
 * @returns True if the value is an array, otherwise false.
 */
export function isArray<T = any>(v: unknown): v is T[] {
  return Array.isArray(v);
}

/**
 * Checks if the value is undefined.
 *
 * @param v The value to check.
 * @returns True if the value is undefined, otherwise false.
 */
export function isUndefined(v: unknown): v is undefined {
  return v === undefined;
}

/**
 * Checks if the value is null.
 *
 * @param v The value to check.
 * @returns True if the value is null, otherwise false.
 */
export function isNull(v: unknown): v is null {
  return v === null;
}

/**
 * Checks if the value is a Promise.
 *
 * @param v The value to check.
 * @returns True if the value is a Promise, otherwise false.
 */
export function isPromise<T = any>(v: unknown): v is Promise<T> {
  return isStrictObject(v) && typeof v.then === "function";
}

/**
 * Checks if the value is NaN (not-a-number).
 *
 * @param v The value to check.
 * @returns True if the value is NaN, otherwise false.
 */
export function isStrictNaN(v: unknown): v is number {
  return isNumber(v) && isNaN(v);
}

/**
 * Checks if the value is an Error object.
 *
 * @param v The value to check.
 * @returns True if the value is an Error object, otherwise false.
 */
export function isError<T extends Error>(v: T): v is T;
export function isError(v: unknown): v is Error;
export function isError(v: unknown): v is Error {
  return v instanceof Error;
}
