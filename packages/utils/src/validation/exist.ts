/**
 * Checks if a value exists and is not null or undefined.
 *
 * @param value The value to check for existence.
 * @returns True if the value exists and is not null or undefined, otherwise false.
 */
export function exist(value: number | null | undefined): value is number;
export function exist(value: string | null | undefined): value is string;
export function exist(value: boolean | null | undefined): value is boolean;
export function exist(value: object | null | undefined): value is object;
export function exist(value: bigint | null | undefined): value is bigint;
export function exist<T>(value: T | null | undefined): value is T;
export function exist(value: unknown): boolean {
  if (value === undefined) {
    return false;
  }
  if (value === null) {
    return false;
  }

  return true;
}

/**
 * Counts the number of existing values in an array, excluding null and undefined.
 *
 * @param values An array of values to count.
 * @returns The number of existing values in the array.
 */
export function countExist<T>(values: Array<T | null | undefined>): number {
  let count: number = 0;
  for (const value of values) {
    if (exist(value)) {
      count++;
    }
  }
  return count;
}

/**
 * Checks if all values in an array exist and are not null or undefined.
 *
 * @param values An array of values to check for existence.
 * @returns True if all values in the array exist and are not null or undefined, otherwise false.
 */
export function existAll<T>(
  values: Array<T | null | undefined>
): values is Array<T> {
  return countExist(values) === values.length;
}

/**
 * Checks if at least one value in an array exists and is not null or undefined.
 *
 * @param values An array of values to check for existence.
 * @returns True if at least one value in the array exists and is not null or undefined, otherwise false.
 */
export function existSome<T>(values: Array<T | null | undefined>): boolean {
  return countExist(values) > 0;
}
