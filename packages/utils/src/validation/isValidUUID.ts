/**
 * Checks if a given string represents a valid UUID (Universally Unique Identifier).
 *
 * @param uuid The string to validate as a UUID.
 * @returns True if the string represents a valid UUID, otherwise false.
 */
export function isValidUUID(uuid: string | any): boolean {
  if (typeof uuid !== "string") {
    return false;
  }
  const r = RegExp(
    "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"
  );
  return r.test(uuid.toLowerCase());
}
