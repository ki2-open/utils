/**
 * Checks if a given string contains lowercase letters.
 *
 * @param password The string to check.
 * @returns True if the string contains lowercase letters, otherwise false.
 */
export function hasLowerCaseLetters(password: string) {
  return password.search(/[a-z]/g) >= 0;
}

/**
 * Checks if a given string contains uppercase letters.
 *
 * @param password The string to check.
 * @returns True if the string contains uppercase letters, otherwise false.
 */
export function hasUpperCaseLetters(password: string): boolean {
  return password.search(/[A-Z]/g) >= 0;
}

/**
 * Checks if a given string contains numbers.
 *
 * @param password The string to check.
 * @returns True if the string contains numbers, otherwise false.
 */
export function hasNumbers(password: string): boolean {
  return password.search(/[0-9]/g) >= 0;
}

/**
 * Checks if a given string contains special characters.
 *
 * @param password The string to check.
 * @returns True if the string contains special characters, otherwise false.
 */
export function hasSpecialLetters(password: string): boolean {
  return password.search(/[^a-zA-Z0-9]/g) >= 0;
}

/**
 * Checks if a given string is shorter than 8 characters.
 *
 * @param password The string to check.
 * @returns True if the string is shorter than 8 characters, otherwise false.
 */
export function isShortPassword(password: string): boolean {
  return password.length < 8;
}

/**
 * Checks if a given string is a strong password.
 *
 * A strong password should be at least 8 characters long and contain characters from at least three of the following categories:
 * lowercase letters, uppercase letters, numbers, and special characters.
 *
 * @param password The string to check.
 * @returns True if the string is a strong password, otherwise false.
 */
export function isStrongPassword(password: string): boolean {
  if (isShortPassword(password)) {
    return false;
  }

  let count: number = 0;
  if (hasLowerCaseLetters(password)) {
    count += 1;
  }
  if (hasUpperCaseLetters(password)) {
    count += 1;
  }
  if (hasNumbers(password)) {
    count += 1;
  }
  if (hasSpecialLetters(password)) {
    count += 1;
  }

  return count >= 3;
}
