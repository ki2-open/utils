import { existType, newObjectId } from "../src";

import { v4 as uuid } from "uuid";

describe("test existType function", () => {
  it("should be valid base type (no array)", () => {
    expect(existType({}, "object")).toBe(true);
    expect(existType(true, "boolean")).toBe(true);
    expect(existType(0, "number")).toBe(true);
    expect(existType(BigInt(0), "bigint")).toBe(true);
    expect(existType("", "string")).toBe(true);
    expect(existType(Symbol(0), "symbol")).toBe(true);
    expect(existType(() => null, "function")).toBe(true);
  });

  it("should be invalid object type", () => {
    expect(existType(undefined, "object")).toBe(false);
    expect(existType(null, "object")).toBe(false);
    expect(existType(true, "object")).toBe(false);
    expect(existType(0, "object")).toBe(false);
    expect(existType(BigInt(0), "object")).toBe(false);
    expect(existType("", "object")).toBe(false);
    expect(existType(Symbol(0), "object")).toBe(false);
    expect(existType(() => null, "object")).toBe(false);
  });

  it("should be invalid boolean type", () => {
    expect(existType(undefined, "boolean")).toBe(false);
    expect(existType(null, "boolean")).toBe(false);
    expect(existType({}, "boolean")).toBe(false);
    expect(existType(0, "boolean")).toBe(false);
    expect(existType(BigInt(0), "boolean")).toBe(false);
    expect(existType("", "boolean")).toBe(false);
    expect(existType(Symbol(0), "boolean")).toBe(false);
    expect(existType(() => null, "boolean")).toBe(false);
  });

  it("should be invalid number type", () => {
    expect(existType(undefined, "number")).toBe(false);
    expect(existType(null, "number")).toBe(false);
    expect(existType({}, "number")).toBe(false);
    expect(existType(true, "number")).toBe(false);
    expect(existType(BigInt(0), "number")).toBe(false);
    expect(existType("", "number")).toBe(false);
    expect(existType(Symbol(0), "number")).toBe(false);
    expect(existType(() => null, "number")).toBe(false);
  });

  it("should be invalid bigint type", () => {
    expect(existType(undefined, "bigint")).toBe(false);
    expect(existType(null, "bigint")).toBe(false);
    expect(existType({}, "bigint")).toBe(false);
    expect(existType(true, "bigint")).toBe(false);
    expect(existType(0, "bigint")).toBe(false);
    expect(existType("", "bigint")).toBe(false);
    expect(existType(Symbol(0), "bigint")).toBe(false);
    expect(existType(() => null, "bigint")).toBe(false);
  });

  it("should be invalid string type", () => {
    expect(existType(undefined, "string")).toBe(false);
    expect(existType(null, "string")).toBe(false);
    expect(existType({}, "string")).toBe(false);
    expect(existType(true, "string")).toBe(false);
    expect(existType(0, "string")).toBe(false);
    expect(existType(BigInt(0), "string")).toBe(false);
    expect(existType(Symbol(0), "string")).toBe(false);
    expect(existType(() => null, "string")).toBe(false);
  });

  it("should be invalid symbol type", () => {
    expect(existType(undefined, "symbol")).toBe(false);
    expect(existType(null, "symbol")).toBe(false);
    expect(existType({}, "symbol")).toBe(false);
    expect(existType(true, "symbol")).toBe(false);
    expect(existType(0, "symbol")).toBe(false);
    expect(existType(BigInt(0), "symbol")).toBe(false);
    expect(existType("", "symbol")).toBe(false);
    expect(existType(() => null, "symbol")).toBe(false);
  });

  it("should be invalid function type", () => {
    expect(existType(undefined, "function")).toBe(false);
    expect(existType(null, "function")).toBe(false);
    expect(existType({}, "function")).toBe(false);
    expect(existType(true, "function")).toBe(false);
    expect(existType(0, "function")).toBe(false);
    expect(existType(BigInt(0), "function")).toBe(false);
    expect(existType("", "function")).toBe(false);
    expect(existType(Symbol(0), "function")).toBe(false);
  });

  it.skip("should be valid ext type (no array)", () => {
    expect(existType(uuid(), "@id")).toBe(true);
    expect(existType(newObjectId(), "@id")).toBe(true);
    expect(existType(0, "@id")).toBe(true);
    expect(existType("", "@id")).toBe(true);

    expect(existType("a@b.c", "@email")).toBe(true);
  });

  it.skip("should be invalid ext type (no array)", () => {
    expect(existType(true, "@id")).toBe(false);
    expect(existType({}, "@id")).toBe(false);

    expect(existType(123, "@email")).toBe(false);
  });

  it.skip("should be valid arrayed type", () => {
    expect(existType(0, ["string", "number"])).toBe(true);
    expect(existType("", ["function", "number", "string", "bigint"]));
    expect(existType(true, ["boolean"]));
  });

  it.skip("should be invalid arrayed type", () => {
    expect(existType(0, ["string", "function"])).toBe(false);
    expect(existType(null, ["string"])).toBe(false);
    expect(existType("test", ["number", "symbol"])).toBe(false);
  });
});
