import { isObjectId, newObjectId, isValidObjectId } from "../src";

import { ObjectID } from "bson";

describe("test object id functions", () => {
  it("should be valid object id", () => {
    expect(isObjectId(new ObjectID())).toBe(true);
    expect(isObjectId(new ObjectID(25))).toBe(true);
    expect(isValidObjectId(new ObjectID())).toBe(true);
    expect(isValidObjectId(new ObjectID().toHexString())).toBe(true);
    expect(isValidObjectId(0)).toBe(true);
  });

  it("should be invalid object id", () => {
    expect(isObjectId(null)).toBe(false);
    expect(isObjectId("test")).toBe(false);
    expect(isObjectId(0)).toBe(false);
    expect(isValidObjectId(false)).toBe(false);
    expect(isValidObjectId("test")).toBe(false);
  });

  it("could generate valid object id", () => {
    expect(isObjectId(newObjectId())).toBe(true);
    expect(isObjectId(newObjectId(null))).toBe(true);
    expect(isObjectId(newObjectId(0))).toBe(true);
    expect(isObjectId(newObjectId("0"))).toBe(true);
  });
});
