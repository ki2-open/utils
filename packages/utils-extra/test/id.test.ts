import { isId, isExtId, newObjectId } from "../src";

import { v4 as uuid } from "uuid";

describe("test id validators", () => {
  it("shoud be valid id", () => {
    expect(isId(0)).toBe(true);
    expect(isId(0, "any")).toBe(true);
    expect(isId(0, "uuid")).toBe(true);
    expect(isId(0, "objectid")).toBe(true);
    expect(isId(0, "both")).toBe(true);
    expect(isId("")).toBe(true);
    expect(isId("", "any")).toBe(true);

    const uid = uuid();
    const suid = String(uid);
    expect(isId(suid)).toBe(true);
    expect(isId(suid, "any")).toBe(true);
    expect(isId(suid, "uuid")).toBe(true);
    expect(isId(suid, "both")).toBe(true);

    const oid = newObjectId();
    const soid = String(oid);
    expect(isId(soid)).toBe(true);
    expect(isId(soid, "any")).toBe(true);
    expect(isId(soid, "objectid")).toBe(true);
    expect(isId(soid, "both")).toBe(true);
  });

  it("should be invalid id", () => {
    expect(isId(true)).toBe(false);
    expect(isId({})).toBe(false);

    expect(isId("", "uuid")).toBe(false);
    expect(isId("", "objectid")).toBe(false);
    expect(isId("", "both")).toBe(false);

    const uid = uuid();
    const suid = String(uid);
    expect(isId(suid, "objectid")).toBe(false);

    const oid = newObjectId();
    const soid = String(oid);
    expect(isId(soid, "uuid")).toBe(false);
  });

  it("should be valid extid", () => {
    expect(isExtId("")).toBe(true);
    expect(isExtId(0)).toBe(true);
    expect(isExtId(newObjectId())).toBe(true);
    expect(isExtId(newObjectId(), "uuid")).toBe(true); // validator only affect string test
  });

  it("shoud be invalid extid", () => {
    expect(isExtId(null)).toBe(false);
    expect(isExtId({})).toBe(false);
  });
});
