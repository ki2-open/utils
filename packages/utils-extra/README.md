# @ki2/utils-extra

- `existType.ts`
  - `existType`
- `id.ts`
  - `isId`
  - `isExtId`
- `ObjectId.ts`
  - `isValidObjectId`
  - `isObjectId`
  - `newObjectId`

## Types

```ts
type Id = string | number;
type ExtId = Id | ObjectID;
```

## Examples

### `existType`

Check if a data exist and had a valid type.

```ts
existType(25, "number"); // result: true
existType(25, "boolean"); // result: false (25 is not a boolean)
existType(null, "number"); // result: false (data does not exist)
existType(undefined, "number"); // result: false (data does not exist)
```

### `isId`

Check if id had a valid format (number or string). String format validators can be `uuid`, `objectid`, `both` (`uuid` or `objectid`) or `any` (any string id). The default value is `any`.
To test `ObjectId` object, use `isExtId` (see below).

```ts
isId("6e1f5c36-2854-412f-94e2-e66ff7858990", "uuid"); // result: true
isId("6e1f5c36-2854-412f-94e2-e66ff7858990", "both"); // result: true
isId("6e1f5c36-2854-412f-94e2-e66ff7858990", "any"); // result: true
isId("6e1f5c36-2854-412f-94e2-e66ff7858990", "objectid"); // result: false

isId("62040751f5f286a8d01f7b64", "objectid"); // result: true
isId("62040751f5f286a8d01f7b64", "both"); // result: true
isId("62040751f5f286a8d01f7b64", "any"); // result: true
isId("62040751f5f286a8d01f7b64", "uuid"); // result: false

isId("hello", "any"); // result: true
isId("hello"); // result: true
isId(25); // result: true
isId(25, "uuid"); // result: true (validator dont apply on numbers)
```

### `isExtId`

Like `isId` but also accept `ObjectId` object (string are still validated by the string validator.

```ts
isExtId(new ObjectId()); // result: true
```

### `isValidObjectId`

Check if an `id` is a valid `ObjectId` (can be object or string).

```ts
isValidObjectId(new ObjectId()); // result: true
isValidObjectId("62040751f5f286a8d01f7b64"); // result: true
isValidObjectId("hello"); // result: false
```

### `isOjectId`

Check if an `id` is an `ObjectId` (string are rejected).

```ts
isValidObjectId(new ObjectId()); // result: true
isValidObjectId("62040751f5f286a8d01f7b64"); // result: false
```

### `newObjectId`

Generate an from a valid `ObjectId` (object or string) or generate a new (random) `ObjectId` if no ObjectId is given.

```ts
newObjectId("62040751f5f286a8d01f7b64"); // result: ObjectId("62040751f5f286a8d01f7b64")
newObjectId(new ObjectId("62040751f5f286a8d01f7b64")); // result: ObjectId("62040751f5f286a8d01f7b64") (copy from the original one)
newObjectId(); // result (example): ObjectId("62040ab2fc57d7f84e5086cc")
```
