import { ObjectID } from "bson";

import type { Nullable } from "@ki2/utils";
import { isObject } from "@ki2/utils";

import type { ExtId } from ".";

export function isValidObjectId(id: any): boolean {
  return ObjectID.isValid(id);
}

export function isObjectId(id: any): id is ObjectID {
  return isObject(id) && isValidObjectId(id);
}

export function newObjectId(id?: Nullable<ExtId>): ObjectID {
  if (isValidObjectId(id)) {
    return new ObjectID(id);
  }
  return new ObjectID();
}
