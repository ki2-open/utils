import { isString, isNumber, isValidUUID } from "@ki2/utils";

import { ExtId, Id, isObjectId } from ".";
import { isValidObjectId } from ".";

export type IsIdStringValidator = "any" | "uuid" | "objectid" | "both";

export function isId(
  id: any,
  validator: IsIdStringValidator = "any"
): id is Id {
  if (isNumber(id)) {
    return true;
  }
  if (isString(id)) {
    if (validator === "uuid") {
      return isValidUUID(id);
    } else if (validator === "objectid") {
      return isValidObjectId(id);
    } else if (validator === "both") {
      return isValidUUID(id) || isValidObjectId(id);
    }
    return true;
  }
  return false;
}

export function isExtId(
  id: any,
  validator: IsIdStringValidator = "any" // validator only affect string test
): id is ExtId {
  if (isObjectId(id)) {
    return true;
  }
  return isId(id, validator);
}
