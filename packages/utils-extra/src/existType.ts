import { exist, isValidEmail } from "@ki2/utils";
import { isExtId } from ".";

type BaseValidationTypes =
  | "object"
  | "boolean"
  | "number"
  | "bigint"
  | "string"
  | "symbol"
  | "function";

type ExtValidationTypes = "@id" | "@email";

export type ValidationTypes = BaseValidationTypes | ExtValidationTypes;

export function existType(data: any, type: ValidationTypes): boolean;
export function existType(data: any, type: Array<ValidationTypes>): boolean;
export function existType(data: any, type: any): boolean;
export function existType(data: any, type: any): boolean {
  if (!exist(data)) {
    return false;
  }

  if (Array.isArray(type)) {
    for (let stype of type) {
      if (existType(data, stype)) {
        return true; // Validate at least one type
      }
    }
  } else {
    if (typeof data === type) {
      return true;
    } else if (typeof type === "object" || typeof type === "function") {
      if (data instanceof type) {
        return true;
      }
    } else if (type === "@id") {
      return isExtId(data);
    } else if (type === "@email") {
      if (data instanceof String || typeof data === "string") {
        return isValidEmail(String(data));
      }
    }
  }

  return false;
}
