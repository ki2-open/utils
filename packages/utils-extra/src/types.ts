import type { ObjectID } from "bson";

export type Id = string | number;
export type ExtId = Id | ObjectID;
