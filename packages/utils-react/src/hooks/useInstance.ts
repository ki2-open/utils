import { useRef } from "react";

export function useInstance<S>(init: S | (() => S)): S {
  const ref = useRef<S | null>(null);

  if (ref.current === null) {
    if (typeof init === "function") {
      const func = init as () => S;
      ref.current = func();
    } else {
      ref.current = init;
    }
  }

  return ref.current;
}
