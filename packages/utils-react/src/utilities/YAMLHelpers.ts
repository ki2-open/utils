import { YAML } from "../externals";

interface YAMLStringifyOpts {
  startWarnMessage?: string;
  skipInvalid?: boolean;
}

export function YAMLStringify(data: any, opts: YAMLStringifyOpts = {}): string {
  if (data !== undefined && data !== null) {
    try {
      return YAML.dump(data, {
        skipInvalid: opts.skipInvalid,
      });
    } catch (e) {
      console.warn(
        `[YAMLStringify/${opts.startWarnMessage}] invalid data to dump causing error`,
        data
      );
    }
  } else {
    let value: "undefined" | "null" = "undefined";
    if (data === null) {
      value = "null";
    }
    console.warn(`[YAMLStringify/${opts.startWarnMessage}] data is ${value}`);
  }
  return "";
}

interface YAMLParseOpts {
  startWarnMessage?: string;
  default?: any;
}

export function YAMLParse(data: string, opts: YAMLParseOpts = {}): any {
  try {
    return YAML.load(data);
  } catch (e) {
    console.warn(
      `[YAMLStringify/${opts.startWarnMessage}] error when parsing data, return default`
    );
    return opts.default;
  }
}
