# @ki2/utils-react

React hooks and utilities functions.

## Externals

This `react-front-utils` package is an opinionated package.
It select some usefull modules and re-export them to centrilize
commonly used features.

### `clx`

Function used to merge classnames (see `clsx` or `classnames` packages)

### `nanoid`

Function to generate a short id. It's a fast alternative to `uuid`.

Exported function :

- `nanoid` : base function (safe & synchronous call)
- `async_nanoid` : asynchronous function
- `unsafe_nanoid`: unsafe function (synchronous)

See `nanoid` package for more informations.

### `qs` (query-string)

Export the `query-string` package into `qs` object.

### `YAML`

Export the `js-yaml` package into `YAML` object.
It's used almost like the `JSON` object.

## Hooks

### `useInstance`

Allow to properly create a `class` instance.

Usage:

```ts
const store = useInstance(() => new MyClass());
```

### `useInterval(callback, delay)`

Allow to regularly (every `delay` milliseconds) call a `callback` function (defined in a component).

```ts
function doSomething() {
  // do something here every second
}

useInterval(doSomething, 1000);
```

The `callback` function may only use a `stop` function as an argument allowing to stop futur calls.

```ts
function doSomething(stop){
  // do something here every second
  if (/* some condition */){
    stop(); // stop interval
  }
}

useInterval(doSomething, 1000)
```

### `useScrollPosition`

Parameters:

- `effect` : function called every time the scroll change (or if any `deps` dependency is changed);
- `deps` : external dependency of the `effect` function (optional);
- `element` : element in which the scroll position is tracked (optional);
- `useWindow` [`boolean`]: force to use full window instead of an `element` (optional)
- `wait` [`number`]: number of milliseconds to wait before the `effect` function is called again (optional).

The `effect` function take 2 parameters : the current scroll position (`currPos`) and the previous scroll position (`prevPos`, from the previous call).

A position (`currPos` or `prevPos`) is an object contaning `x` and `y` positions (scroll from `x` and `y` axis respectively).

Usage (example) :

```ts
const [hide, setHide] = useState(true);

useScrollPosition(
  (curr, prev) => {
    const isHide = curr.y < 250;
    if (isHide !== hide) {
      setHide(isHide);
    }
  },
  [hide],
  contentRef // Reference to the content we looking for
);
```

## Utilities

### `download`

The `download` function allow to download a locally generated file. The content is writed into a string object and browser _download_ it as a file. The file name is given by the first `filename` parameter.

```ts
function download(filename: string, content: string);
```

### handleNext functions

#### `generateNext`

Generate a path with the `search` string.

```ts
function generateNext(pathname: string, search?: string): string;
```

#### `getNext`

Extract the `next` parameter from query string.

```ts
interface INextReturn {
  pathname: string;
  search?: string;
}
```

```ts
function getNext(search?: string): INextReturn;
```

### YAML helpers

```ts
interface YAMLStringifyOpts {
  startWarnMessage?: string;
  skipInvalid?: boolean;
}
```

#### `YAMLStringify`

Stringify data to YAML (handle errors).

```ts
function YAMLStringify(data: any, opts: YAMLStringifyOpts = {}): string;
```

#### `YAMLParse`

Parse YAML to string (handle errors).

```ts
function YAMLParse(data: string, opts: YAMLParseOpts = {}): any;
```
